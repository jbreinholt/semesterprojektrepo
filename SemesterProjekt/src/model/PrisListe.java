package model;

import java.util.HashMap;

public class PrisListe {
	private String listeNavn;
	private HashMap<Produkt, Double> liste = new HashMap<>();
	
	public PrisListe(String navn) {
		listeNavn = navn;
	}
	
	public void addToPrisliste(Produkt produkt, double pris) {
		liste.put(produkt, pris);
	}
	
	public void printPrisListe() {
		System.out.println("\n" + listeNavn);
		for (Produkt p : liste.keySet()) {
			String key = p.toString();
			Double value = liste.get(p);
			System.out.println(p.getType());
			System.out.printf("%25s%15.2f\n", key, value);
//			System.out.printf(key + "%25.2f\n", value);
		}
	}
	
	public double getPris(Produkt produkt) {
		return liste.get(produkt);
	}

	public String getListeNavn() {
		return listeNavn;
	}

	public void setListeNavn(String navn) {
		listeNavn = navn;
	}
	
	
	
}
