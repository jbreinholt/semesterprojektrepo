package model;

public class MainApp {
	public static void main(String[] args) {
	
		PrisListe pl1 = new PrisListe("Butik");
		PrisListe pl2 = new PrisListe("Fredagsbar");
		
		ProduktType flaske = new ProduktType("Flaskeøl, 60 cl.");
		Produkt fKloster = new Produkt(flaske, "Klosterbryg");
		Produkt fBrown = new Produkt(flaske, "Sweet Georgia Brown");
		Produkt fPilsner = new Produkt(flaske, "Extra Pilsner");
		
		pl1.addToPrisliste(fKloster, 15);
		pl1.addToPrisliste(fBrown, 25);
		pl1.addToPrisliste(fPilsner, 45);
		
		pl1.printPrisListe();
//		pl2.printPrisListe();
		
		Ordre o1 = new Ordre(pl1);
		OrdreLinje ol1 = new OrdreLinje(o1, fPilsner, 2);
		o1.addLinje(ol1);
		OrdreLinje ol2 = new OrdreLinje(o1, fKloster, 7);
		o1.addLinje(ol2);
		
		System.out.println(ol1.getProdukt().toString() + ": " + ol1.getLinjePris());
		System.out.println(ol2.getProdukt().toString() + ": " + ol2.getLinjePris());
		System.out.println("Total Pris: " + o1.getTotalPris());
	}
}
