package model;

public class Produkt {
	private String navn;
	private ProduktType produktType;
	
	public Produkt(ProduktType produktType, String navn) {
		this.navn = navn;
		this.produktType = produktType;
	}
	
	public String getNavn() {
		return navn;
	}
	
	public ProduktType getType() {
		return produktType;
	}
	
	@Override
	public String toString() {
		return navn;
	}
}
