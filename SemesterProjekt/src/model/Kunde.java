package model;

public class Kunde {
	private String navn;
	
	public Kunde (String navn) {
		this.navn = navn;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}
}
