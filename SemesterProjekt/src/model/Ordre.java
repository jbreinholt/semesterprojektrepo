package model;

import java.util.ArrayList;

public class Ordre {
	private PrisListe prisListe;
	private ArrayList<OrdreLinje> ordreLinjer = new ArrayList<>();
	private int ordreNummerGenerator = 1;
	private int ordreNummer;
	
	public Ordre(PrisListe prisListe) {
		this.prisListe = prisListe;
		ordreNummer = ordreNummerGenerator;
		ordreNummerGenerator++;
	}

	public int getOrdreNummer() {
		return ordreNummer;
	}

	public void setOrdreNummer(int ordreNummer) {
		this.ordreNummer = ordreNummer;
	}
	
	public PrisListe getPrisListe() {
		return prisListe;
	}
	
	public void addLinje(OrdreLinje ordreLinje) {
		ordreLinjer.add(ordreLinje);
	}
	
	public ArrayList<OrdreLinje> getOrdreLinjer() {
		return new ArrayList<OrdreLinje>(ordreLinjer);
	}
	
	public double getTotalPris() {
		int result = 0;
		for (OrdreLinje ordreLinje : ordreLinjer) {
			result += prisListe.getPris(ordreLinje.getProdukt()) * ordreLinje.getAntal();
		}
		return result;
	}
}
