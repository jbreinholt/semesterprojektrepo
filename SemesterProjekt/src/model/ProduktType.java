package model;

public class ProduktType {
	private String navn;
	
	public ProduktType(String navn) {
		this.navn = navn;
	}
	
	@Override
	public String toString() {
		return navn;
	}
}
