package model;

public class OrdreLinje {
	private int antal;
	private Ordre ordre;
	private Produkt produkt;

	public OrdreLinje(Ordre ordre, Produkt produkt, int antal) {
		this.antal = antal;
		this.ordre = ordre;
		this.produkt = produkt;
	}
	
	public int getAntal() {
		return antal;
	}
	
	public void setAntal(int antal) {
		this.antal = antal;
	}
	
	public Ordre getOrdre() {
		return ordre;
	}
	
	public Produkt getProdukt() {
		return produkt;
	}
	
	public Double getLinjePris() {
		double result = 0;
		result += getOrdre().getPrisListe().getPris(produkt) * antal;
		return result;
	}
}
